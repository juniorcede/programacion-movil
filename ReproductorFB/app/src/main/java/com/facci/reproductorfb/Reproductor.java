package com.facci.reproductorfb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;

public class Reproductor extends AppCompatActivity {

    ImageButton btnPlay,btnNext,btnPrev;
    SeekBar barraProgreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reproductor);

        btnPlay = (ImageButton)findViewById(R.id.play);
        btnNext = (ImageButton)findViewById(R.id.next);
        btnPrev = (ImageButton)findViewById(R.id.prev);
        barraProgreso = (SeekBar)findViewById(R.id.barra);

    }


    public void ClickBtn(View view){
        int button = view.getId();
        switch (button){
            case R.id.play:
                break;
            case R.id.prev:
                break;
            case R.id.next:
                break;
        }
    }

}
