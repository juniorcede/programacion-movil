package com.facci.reproductorfb;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class MainActivityFB extends AppCompatActivity {

    ListView lista;
    String [] items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_fb);
        lista = (ListView)findViewById(R.id.Playlist);
        final ArrayList<File> Canciones = BuscarCanciones(Environment.getExternalStorageDirectory());
        items = new String[Canciones.size()];
        for (int i=0; i<Canciones.size(); i++){
            items[i]= Canciones.get(i).getName().toString().replace(".mp3","").replace(".wav","").toLowerCase();
        }

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getApplicationContext(),R.layout.canciones,R.id.textView,items);
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener( new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent , View view , int position ,long id)
            {
                startActivity(new Intent(getApplicationContext(),Reproductor.class).putExtra("pos",position).putExtra("canciones",Canciones));
            }
        });
    }

    public ArrayList<File> BuscarCanciones(File root){
        ArrayList<File> Canciones = new ArrayList<File>();
        File[] archivosmp3 = root.listFiles();
        for (File archivo : archivosmp3){
            if (archivo.isDirectory() && !archivo.isHidden()){
                Canciones.addAll(BuscarCanciones(archivo));
            }
            else{
                if (archivo.getName().endsWith("mp3") || archivo.getName().endsWith("wav")){
                    Canciones.add(archivo);
                }
            }
        }
        return Canciones;
    }
}
