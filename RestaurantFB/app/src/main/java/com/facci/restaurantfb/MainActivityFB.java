package com.facci.restaurantfb;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivityFB extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_fb);
    }

    public void click1(View v){
        Toast.makeText(MainActivityFB.this,"Ceviche de Camaron",Toast.LENGTH_SHORT).show();
    }
    public void click2(View v){
        Toast.makeText(MainActivityFB.this,"Corvina Frita con Patacones",Toast.LENGTH_SHORT).show();
    }
    public void click3(View v){
        Toast.makeText(MainActivityFB.this,"Chicharron de Pescado con Patacones",Toast.LENGTH_SHORT).show();
    }
    public void click4(View v){
        Toast.makeText(MainActivityFB.this,"Conchas Asadas",Toast.LENGTH_SHORT).show();
    }
    public void click5(View v){
        Toast.makeText(MainActivityFB.this,"Arroz con Menestra y Carne Frita",Toast.LENGTH_SHORT).show();
    }
    public void click6(View v){
        Toast.makeText(MainActivityFB.this,"Humitas",Toast.LENGTH_SHORT).show();
    }
    public void click7(View v){
        Toast.makeText(MainActivityFB.this,"Guatita con Arroz",Toast.LENGTH_SHORT).show();
    }
    /* no tener cuenta esta parte del codigo :v
    public void click8(View v){
        int button = v.getId();
        Toast.makeText(MainActivityFB.this, String.format("el boton es" + button),Toast.LENGTH_SHORT).show();
        Toast.makeText(MainActivityFB.this,"Guatita con Arroz",Toast.LENGTH_SHORT).show();
    }*/
}
