package com.facci.conversorfb;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivityFB extends AppCompatActivity {

    final String[] items = new String[]{"DOLAR","EURO","PESO MEXICANO"};

    private Spinner monedaActualSp;
    private Spinner monedaCambioSp;
    private EditText valorText;
    private TextView resultadoTV;

    final private double DolarEuro = 0.87;
    final private double PesoDolar = 0.54;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_fb);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,items);

        monedaActualSp = (Spinner) findViewById(R.id.mActualSp);
        monedaActualSp.setAdapter(adaptador);
        monedaCambioSp = (Spinner) findViewById(R.id.mCambioSp);

        SharedPreferences monedas = getSharedPreferences("Monedas", Context.MODE_PRIVATE);
        String mActual=monedas.getString("MonedaActual","");
        String mCambio=monedas.getString("MonedaCambio","");
        if (!mActual.equals("")){
            int indice1 = adaptador.getPosition(mActual);
            monedaActualSp.setSelection(indice1);
        }
        if (!mCambio.equals("")){
            int indice = adaptador.getPosition(mCambio);
            monedaCambioSp.setSelection(indice);
        }




    }

    public void clickConvertir(View v ){
        monedaActualSp = (Spinner) findViewById(R.id.mActualSp);
        monedaCambioSp = (Spinner) findViewById(R.id.mCambioSp);
        valorText = (EditText) findViewById(R.id.vCambio);
        resultadoTV = (TextView) findViewById(R.id.resultadoText);

        String monedaActual= monedaActualSp.getSelectedItem().toString();
        String monedaCambio= monedaCambioSp.getSelectedItem().toString();

        double valorCambio = Double.parseDouble(valorText.getText().toString());

        double conversion = Convertir(monedaActual,monedaCambio,valorCambio);

        if (conversion > 0){
            resultadoTV.setText(String.format("Por  %5.2f%s  usted recibirá %5.2f%s",valorCambio," "+monedaActual,conversion," "+monedaCambio));
            valorText.setText("");

            SharedPreferences monedas = getSharedPreferences("Monedas", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = monedas.edit();
            editor.putString("monedaActual",monedaActual);
            editor.putString("monedaCambio",monedaCambio);
            editor.commit();
        }
        else{
            resultadoTV.setText(String.format("Usted recibirá"));
            Toast.makeText(this,"Las opciones elegidas no tienen un factor de conversion",Toast.LENGTH_SHORT).show();
        }

    }

    private double Convertir(String monedaActual, String monedaCambio, double valor){
        double resultadoC= 0;
        switch (monedaActual){
            case "DOLAR":
                if (monedaCambio.equals("EURO")){
                    resultadoC = valor * DolarEuro;
                }
                else if(monedaCambio.equals("PESO MEXICANO")){
                    resultadoC = valor / PesoDolar;
                }
                break;
            case "EURO":
                if (monedaCambio.equals("DOLAR")){
                    resultadoC = valor / DolarEuro;
                }
                break;
            case "PESO MEXICANO":
                if(monedaCambio.equals("DOLAR")){
                    resultadoC = valor * PesoDolar;
                }
                break;
        }

        return resultadoC;
    }
}
