package com.facci.camarafb;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivityFB extends AppCompatActivity {

    Button boton;
    Button boton2;
    ImageView image;
    Bitmap bitma;
    Intent cosita;
    File foto;
    String namefile;
     private final String rfotos = Environment.getExternalStorageDirectory().getAbsolutePath()+"/DCIM//AlmbumFB/";;
    private File file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        file = new File(rfotos);
        Toast.makeText(this,rfotos,Toast.LENGTH_LONG).show();
         if (!file.exists()) {
             file.mkdirs();
            //Toast.makeText(this,"se aha creado el directorio"+rfotos,Toast.LENGTH_LONG).show();
         }
        setContentView(R.layout.activity_main_activity_fb);
        boton = (Button)findViewById(R.id.fotoBT);
        boton2 = (Button)findViewById(R.id.almacenarBT);
        image=(ImageView)findViewById(R.id.imagenCapturadaIV);

    }

    public void click(View v){
        int identificador = v.getId();
        switch (identificador){
            case R.id.fotoBT:
                    cosita = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    namefile = rfotos+getCode() + ".jpg";
                    startActivityForResult(cosita,0);

                break;
            case R.id.almacenarBT:
                foto = new File(rfotos,namefile );
                Uri uriSavedImage = Uri.fromFile(foto);
                cosita.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
                Toast.makeText(this,"Foto guardada con exito",Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requesCode, int requesResult, Intent data){
        super.onActivityResult(requesCode,requesResult,data);
        if(requesResult == Activity.RESULT_OK){
            Bundle extra = data.getExtras();
            bitma = (Bitmap)extra.get("data");
            image.setImageBitmap(bitma);

        }
    }

    @SuppressLint("SimpleDateFormat")
    private String getCode()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
        String date = dateFormat.format(new Date() );
        String photoCode = "pic_" + date;
        return photoCode;
    }
}
